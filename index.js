"use strict";

const LIST_CHARACTERS = {
    'ァ': 'ｧ',
    'ア': 'ｱ',
    'ィ': 'ｨ',
    'イ': 'ｲ',
    'ゥ': 'ｩ',
    'ウ': 'ｳ',
    'ェ': 'ｪ',
    'エ': 'ｴ',
    'ォ': 'ｫ',
    'オ': 'ｵ',
    'カ': 'ｶ',
    'ガ': 'ｶﾞ',
    'キ': 'ｷ',
    'ギ': 'ｷﾞ',
    'ク': 'ｸ',
    'グ': 'ｸﾞ',
    'ケ': 'ｹ',
    'ゲ': 'ｹﾞ',
    'コ': 'ｺ',
    'ゴ': 'ｺﾞ',
    'サ': 'ｻ',
    'ザ': 'ｻﾞ',
    'シ': 'ｼ',
    'ジ': 'ｼﾞ',
    'ス': 'ｽ',
    'ズ': 'ｽﾞ',
    'セ': 'ｾ',
    'ゼ': 'ｾﾞ',
    'ソ': 'ｿ',
    'ゾ': 'ｿﾞ',
    'タ': 'ﾀ',
    'ダ': 'ﾀﾞ',
    'チ': 'ﾁ',
    'ヂ': 'ﾁﾞ',
    'ッ': 'ｯ',
    'ツ': 'ﾂ',
    'ヅ': 'ﾂﾞ',
    'テ': 'ﾃ',
    'デ': 'ﾃﾞ',
    'ト': 'ﾄ',
    'ド': 'ﾄﾞ',
    'ナ': 'ﾅ',
    'ニ': 'ﾆ',
    'ヌ': 'ﾇ',
    'ネ': 'ﾈ',
    'ノ': 'ﾉ',
    'ハ': 'ﾊ',
    'バ': 'ﾊﾞ',
    'パ': 'ﾊﾟ',
    'ヒ': 'ﾋ',
    'ビ': 'ﾋﾞ',
    'ピ': 'ﾋﾟ',
    'フ': 'ﾌ',
    'ブ': 'ﾌﾞ',
    'プ': 'ﾌﾟ',
    'ヘ': 'ﾍ',
    'ベ': 'ﾍﾞ',
    'ペ': 'ﾍﾟ',
    'ホ': 'ﾎ',
    'ボ': 'ﾎﾞ',
    'ポ': 'ﾎﾟ',
    'マ': 'ﾏ',
    'ミ': 'ﾐ',
    'ム': 'ﾑ',
    'メ': 'ﾒ',
    'モ': 'ﾓ',
    'ャ': 'ｬ',
    'ヤ': 'ﾔ',
    'ュ': 'ｭ',
    'ユ': 'ﾕ',
    'ョ': 'ｮ',
    'ヨ': 'ﾖ',
    'ラ': 'ﾗ',
    'リ': 'ﾘ',
    'ル': 'ﾙ',
    'レ': 'ﾚ',
    'ロ': 'ﾛ',
    'ヮ': '',
    'ワ': 'ﾜ',
    'ヲ': 'ｦ',
    'ン': 'ﾝ',
    'ヴ': 'ｳﾞ',
    '・': '･',
    'ー': 'ｰ',
    '。': '.',
    '「': '｢',
    '」': '｣'
}

const toHalfWidth = (string) => {
    try {
        string              = string.normalize('NFKC')
        let characters      = getCharacters(string);
        let halfWidthString = ''
        characters.forEach(character => {
            halfWidthString += mapToHalfWidth(character);
        });
        return halfWidthString;
    } catch (exception) {

    }
};

const toFullWidth = (string) => {
    try {
        string = string.replace(/[!-~]/g, fullwidthChar => String.fromCharCode(fullwidthChar.charCodeAt(0) + 0xfee0));
        let characters      = getCharacters(string);
        let fullWidthString = ''
        characters.forEach(character => {
            fullWidthString += mapToFullWidth(character);
        });
        return fullWidthString;
    } catch (exception) {

    }
};

const getCharacters = (string) => {
    return string.split('');
}

const mapToHalfWidth = (character) => {
    if (typeof LIST_CHARACTERS[character] === 'undefined') {
        return character;
    } else {
        return LIST_CHARACTERS[character];
    }
}

const mapToFullWidth = (character) => {
    let fullWidthCharacter = Object.keys(LIST_CHARACTERS).find(key => LIST_CHARACTERS[key] === character)
    if (typeof fullWidthCharacter === 'undefined') {
        return character;
    } else {
        return fullWidthCharacter
    }
}

module.exports = function (options) {
    this.toHalfWidth = toHalfWidth;
    this.toFullWidth = toFullWidth;
};